@extends('layouts.app')
@section('content')

<h1>Create Post</h1>
{!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="from-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title...']) !!}
    </div>
<br>
    <div class="from-group">
        {!! Form::label('body', 'Body') !!}
        {!! Form::textarea('body', '', ['class' => 'form-control', 'id' => 'article-ckeditor', 'placeholder' => 'Body Post here...']) !!}
    </div>
<br>
<div class="form-group">
    {!! Form::file('cover_image') !!}
</div>
<br>
    <div class="form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}
@endsection
