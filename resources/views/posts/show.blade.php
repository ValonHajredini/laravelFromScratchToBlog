@extends('layouts.app')
@section('content')

    <p>
        <a href="/posts" class="btn btn-outline-dark float-left"> <- Go back</a>
        @if(!Auth::guest())
            @if(Auth::user()->id == $post ->user_id)
                {!! Form::open(['action' => ['PostsController@destroy',$post->id ], 'method' => 'POST', 'class' => 'right']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger float-right']) !!}
                {!! Form::hidden('_method', 'DELETE') !!}
                {!! Form::close() !!}
                 <a href="/posts/{!! $post->id !!}/edit" class="btn btn-outline-primary float-right" style="margin-right: 4px;">Edit</a>
            @endif
        @endif
    </p>
    <br>
    <br>
    <h1>{{$post->title}}</h1>
    <img src="/storage/cover_images/{!! $post->cover_image !!}" width="100%" alt=""><br><br>
    <small>{!!  $post->body !!}</small>
    <br>
    <small>Writen on :    <date>{{$post->created_at}} </date> By: {!! $post->user->name !!}</small>
    <hr>

@endsection
