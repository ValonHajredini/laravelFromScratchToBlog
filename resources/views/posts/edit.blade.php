@extends('layouts.app')
@section('content')

    <h1>Edit: <b> {!! $post->title !!}</b></h1>
    {!! Form::open(['action' => ['PostsController@update',$post->id ], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="from-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title...', 'value' => $post->title]) !!}
    </div>
    <br>
    <div class="from-group">
        {!! Form::label('body', 'Body') !!}
        {!! Form::textarea('body', $post->body, ['class' => 'form-control', 'id' => 'article-ckeditor', 'placeholder' => 'Body Post here...']) !!}
    </div>
    <br>
    <div class="form-group">
        {!! Form::file('cover_image') !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::hidden('_method', 'PUT') !!}
    {!! Form::close() !!}
@endsection
