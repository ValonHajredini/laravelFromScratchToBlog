@extends('layouts.app')
@section('content')

    @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="well">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <img src="/storage/cover_images/{!! $post->cover_image !!}" width="100%" alt="">
                        </div>
                        <div class="col-md-8  col-sm-8">
                            <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                            <small>Writen on :    <date>{{$post->created_at}} </date> By: {!! $post->user->name !!}</small>
                        </div>
                    </div>

                </div>
            @endforeach
            {{$posts->links()}}
    @else
        <p>No posts Found</p>
    @endif
    @endsection
