@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4 style="padding-bottom: 10px;">Your Blog posts <a href="/posts/create"  class="btn btn-primary float-right ">Create posts</a></h4>
                        @if(count($posts) > 0)
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($posts as $post)
                            <tr>
                                <td><h2><a href="/posts/{!! $post->id !!}">{!! $post->title !!}</a></h2></td>

                                <td><a href="/posts/{!! $post->id !!}/edit" class="btn btn-outline-primary float-right" style="margin-right: 4px;">Edit</a></td>
                                <td>
                                    {!! Form::open(['action' => ['PostsController@destroy',$post->id ], 'method' => 'POST', 'class' => 'right']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger float-right']) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                        @else
                            <p>No post created yet!!!</p>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
