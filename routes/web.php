<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Home Rute
// -- Route::get('/hello',function(){ return 'hello world'; });
// Dinamic route
// --Route::get('users/{id}', function ($id) { return 'User: ' . $id; });

// Root dir
Route::get('/', function () { return view('welcome'); });

Route::get('/', 'PagesController@index');

// About
Route::get('/about', 'PagesController@about');

// Services
Route::get('/services', 'PagesController@services');


// Posts routes
Route::resource('posts','PostsController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
