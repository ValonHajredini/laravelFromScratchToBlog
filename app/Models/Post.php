<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    //Primary key
    public $primaryKey = 'id';

    // Tiumestamp;
    public $timestamps = true;
    public function user(){
        return $this->belongsTo('App\User');
    }
}
