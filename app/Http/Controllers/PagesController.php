<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Index
    public function index(){
        $title = 'Welcome t o laravel!';
//        return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);
    }

    // About
    public function about(){
        $title = 'About us';
        return view('pages.about')->with('title', $title);
    }

    // Resources
    public function services(){
        $data = array(
            'title'     => 'Serviices',
            'services'  => ['Web Design', 'WebDevelopment', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}
