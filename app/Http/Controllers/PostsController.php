<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use function PhpParser\filesInDir;




class PostsController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts =  Post::orderBy('id', 'dsc')->take(1)->get();
        //$posts =  Post::orderBy('id', 'dsc')->get();
        //$posts = DB::select('SELECT * FROM posts order by id DESC');


        $posts =  Post::orderBy('updated_at', 'dsc')->paginate(8);
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'         => 'required',
            'body'          => 'required',
            'cover_image'   => 'image|nullable|max:1999'
        ]);
        //
        // Heandel file upload
//        return $request->hasFile('cover_image');
        if ($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithEXT = $request->file('cover_image')->getClientOriginalName();

            // Get just Filename
            $filename = pathinfo($filenameWithEXT, PATHINFO_FILENAME);

            // Get just extension
            $extension = $request-> file('cover_image')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename .'_'.time().".".$extension;

            // Upload Imagw
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        } else {
            $fileNameToStore = 'noImage.jpg';
        }
        // Create Post
        $post = new Post();
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();
        return redirect('/posts')->with('success', 'Post Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post =  Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $post =  Post::find($id);

        // Check for current user
        if (Auth::user()->id != $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'         => 'required',
            'body'          => 'required',
            'cover_image'   => 'image|nullable|max:1999'
        ]);
        // Heandel file upload
//        return $request->hasFile('cover_image');
        if ($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithEXT = $request->file('cover_image')->getClientOriginalName();

            // Get just Filename
            $filename = pathinfo($filenameWithEXT, PATHINFO_FILENAME);

            // Get just extension
            $extension = $request-> file('cover_image')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename .'_'.time().".".$extension;

            // Upload Imagw
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }


        $post = Post::find($id);
        // Create Post
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        if($request->hasFile('cover_image')){ $post->cover_image = $fileNameToStore; }
        $post->save();

        return redirect('/posts/'. $id)->with('success', 'Post Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        // Check for current user
        if (Auth::user()->id != $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        if ($post->cover_image != 'noImage.jpg'){
           // Delete Image
            Storage::delete('public/cover_images/'.$post->cover_image);
        }

        if ($post->delete()){
            return redirect('/posts/')->with('success', 'Post Deleted');
        } else {
            return redirect('/posts/'. $id)->with('error', 'Post wos not delted');
        }
    }
}
